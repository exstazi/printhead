#ifndef PRINTHEAD_H
#define PRINTHEAD_H

#include <QString>
#include <QByteArray>
#include "ui/uigeneratedata.h"
#include "codeheadermessages.h"
#include "communicate.h"
#include "json.h"

#define INCH_TO_MM 1 / 25.4
#define SIZE_BUFFER 65535
#define VOLUME 0.00000045


typedef unsigned char uint8_t;
typedef unsigned int uint32_t;

class PrintHead: public QObject
{
    Q_OBJECT
public:
    PrintHead(Communicate* communicate);

    void writeData(QByteArray* data);
    void writeFirmware(QByteArray* data);
    void updateFirmware();
    void print();
    void printdicals();
    void printWithEncoder();
    void stopPrint();

    void setTypeHead(uint8_t type);
    void setResolution(uint32_t encoderResolution, uint32_t printResolution);
    void setPrintPeriod(uint32_t printPeriod);
    void setHeatingTime(bool delayBeforePrint, uint32_t heatingTime);
    void setPrintWithTrigger(bool printWithTrigger);
    void setTaskHeater(bool workHeater, int task);
    void setIpAddress(QString addr);

    void queryUpdate();
    void reboot();
    void shutdown();
    void close();

    bool isDelayBeforePrint();
    bool isPrintWithTrigger();
    bool isWorkHeater();

    uint8_t getTypeHead();
    uint32_t getEncoderResolution();
    uint32_t getPrintResolution();
    uint32_t getPrintPeriod();
    uint32_t getHeatingTime();
    int getTaskHeater();

    uint32_t getCountNozzle();
    uint32_t getCountPrint();
    uint32_t getConsuption();
    double getTemperature(uint8_t sensor);
    uint8_t getState();

    void writeAnotherCommand(uint8_t command, uint32_t param);

    enum State {
        WaitToConnect = 0x01,
        Connect = 0x02,
        WarmingUp = 0x03,
        Error = 0x04,
        ReadyPrint = 0x05,
        Printing = 0x06
    };

protected:
    void writeParam(uint8_t command, uint32_t param);

protected:
    uint8_t _typeHead;
    uint32_t _encoderResolution;
    uint32_t _printResolution;
    uint32_t _printPeriod;
    uint32_t _heatingTime;
    int _taskHeater;
    bool _delayBeforePrint;
    bool _printWithTrigger;
    bool _workHeater;
    QString _ipAddr;


    bool _isSynchronized;
    uint32_t _countPrint;
    uint32_t _consumption;
    double* _temperature;

    uint8_t _state;
    Communicate* _communicate;

    uint32_t _currentConsuption;

signals:
    void changeState(uint8_t state);
    void changePinState(uint8_t state);
    void update();

protected slots:
    void stateConnect(bool state);
    void readyCommand(uint8_t command, uint32_t param);
};

#endif // PRINTHEAD_H
