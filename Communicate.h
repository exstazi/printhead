#ifndef COMMUNICATE
#define COMMUNICATE

#include <QObject>
#include <QByteArray>

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;

class Communicate: public QObject
{
    Q_OBJECT
public:
    virtual void connectToHead() = 0;
    virtual void disconnectFromHead() = 0;
    virtual void writeCommand(uint8_t command,
                              uint32_t param = 0, QByteArray* data = NULL) = 0;
    bool isConnect() { return _stateConnect; }

protected:
    bool _stateConnect;

signals:
    void stateConnect(bool state);
    void readyCommand(uint8_t command, uint32_t param);
};

#endif // COMMUNICATE

