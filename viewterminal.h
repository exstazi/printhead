#ifndef VIEWTERMINAL_H
#define VIEWTERMINAL_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QTimer>

#include "json.h"

#include "ui/uiloadimage.h"
#include "ui/uiselectchannel.h"

#include "socketcommunicate.h"
#include "printhead.h"

#define FN_CONFIGURATION "configuration.json"

namespace Ui {
class ViewTerminal;
}

class ViewTerminal : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewTerminal(QWidget *parent = 0);
    ~ViewTerminal();

protected:
    Ui::ViewTerminal *ui;
    UILoadImage *_image;
    UISelectChannel *_channel;

    PrintHead* _head = NULL;
    Communicate* _communicate = NULL;
    QTimer* _timer;

    void resizeEvent(QResizeEvent *event);
    void updateConfigurationHeadUI();
    void loadConfiguration();
    void saveConfiguration();

protected slots:
    void loadPictures();
    void connectToRPI();
    void writeToRPI();
    void printToPRI();
    void printPridicoals();
    void printEncoder();
    void stopPrint();
    void rebootHead();
    void shutdownHead();

    void changeMode();
    void changePageToConfigure();
    void changePageToHelp();
    void changePageToUpdateFirmvare();
    void checkConfiguration();
    void changeStatusPrintHead(bool status);
    void changeStateHead(uint8_t state);
    void changePinState(uint8_t state);

    void changeTriggerHeatingTime(bool status);
    void changeTriggerTaskHeater(bool status);

    void onClickFunction1Btn();
    void onClickFunction2Btn();

    void timerTick();
    void updateParamHead();

    void updateFirmware();
};

#endif // VIEWTERMINAL_H
