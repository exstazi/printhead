#ifndef UISELECTCHANNEL_H
#define UISELECTCHANNEL_H

#include "ui/uigeneratedata.h"
#include <QVector>
#include <QPainter>
#include <QMouseEvent>

struct Node
{
    int x;
    int y;
    bool isOpen;

    Node( int _x = 0, int _y = 0 )
    {
        x = _x;
        y = _y;
        isOpen = false;
    }
};


class UISelectChannel : public UIGenerateData
{
    Q_OBJECT
public:
    UISelectChannel( unsigned int height = 128 );
    ~UISelectChannel();
    void clear();
    void setCheckAll();
    void setCheckBoard();
    QByteArray* getData();

    void setHeight(unsigned int height);

protected:

    int index;
    bool checkAll;
    unsigned int _rows;
    QVector<Node> list;

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void createList();
};

#endif // UISELECTCHANNEL_H
