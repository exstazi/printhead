#include "uiloadimage.h"

UILoadImage::UILoadImage(unsigned int height)
{
    _height = height;
    _data = NULL;
}

UILoadImage::~UILoadImage()
{
    delete _pixmap;
    delete _data;
}

void UILoadImage::loadImage(QString fileName)
{
    _pixmap = new QPixmap( fileName );
    _width = _pixmap->width();
    if (_data) {
        delete _data;
    }
    _data = NULL;

    this->setMinimumWidth( _pixmap->width() );
    this->setMinimumHeight( _pixmap->height() );
}

void UILoadImage::paintEvent(QPaintEvent *)
{
    if (!_pixmap) {
        return ;
    }

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush((QBrush(QColor(0,0,0))));

    p.drawPixmap(0,0,*_pixmap);
    if ((unsigned int)_pixmap->height() < _height) {
        p.setBrush( Qt::white );
        p.drawRect( 0, _pixmap->height(), _pixmap->width(), _height - _pixmap->height());
    }
    p.setPen( Qt::red );
    p.drawLine( 0, _height, _width, _height );
}

void UILoadImage::wheelEvent(QWheelEvent *event)
{
    if( _pixmap->height() + event->delta() / 10 >= 128 )
    {
        _pixmap = new QPixmap( _pixmap->scaled( _pixmap->width() + event->delta() / 10, _pixmap->height() + event->delta() / 10 ) );
        this->setMinimumWidth( _pixmap->width() );
        this->setMinimumHeight( _pixmap->height() );
        update();
    }
}

QByteArray* UILoadImage::getData()
{
    if (!_pixmap) {
        qDebug() << "Img not defined";
        return NULL;
    }
    if (_data) {
        return _data;
    }
    _data = new QByteArray();
    QImage image = _pixmap->toImage();
    char byte;
    unsigned int colorPixel = 0x00;
    for (unsigned long i = 0; i < _width; ++i ) {
        for (unsigned int j = 0; j < _height; ++j ) {

            if (image.height() > j ) {
                colorPixel = image.pixel( i,j );
            } else {
                colorPixel = 0xffffffff;
            }

            if (colorPixel != 0xffffffff) {
                byte = (byte<<1) | 0x01;
            } else {
                byte <<= 0x01;
            }
            if (j % 8 == 0 && j != 0) {
                _data->append( byte );
            }
        }
        _data->append( byte );
    }

    return _data;
}
