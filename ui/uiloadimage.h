#ifndef UILOADIMAGE_H
#define UILOADIMAGE_H

#include "ui/uigeneratedata.h"

#include <QPainter>
#include <QPixmap>
#include <QWheelEvent>

class UILoadImage : public UIGenerateData
{
    Q_OBJECT
public:
    UILoadImage( unsigned int height = 128 );
    ~UILoadImage();

    void loadImage(QString fileName);
    QByteArray* getData();

protected:
    void paintEvent(QPaintEvent *);
    void wheelEvent(QWheelEvent *event);

protected:
    QPixmap *_pixmap;
};

#endif // UILOADIMAGE_H
