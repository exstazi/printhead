#include "uiselectchannel.h"
#include <QDebug>

UISelectChannel::UISelectChannel(unsigned int height)
{
    this->setMinimumWidth( 100 + 7 * 120 );
    this->setMinimumHeight( 100 + 7 * 40 );
    setMouseTracking( true );
    index = -1;
    checkAll = true;
    setHeight( height );
    _data = new QByteArray();
}

UISelectChannel::~UISelectChannel()
{
    delete _data;
    list.clear();
}

void UISelectChannel::setHeight(unsigned int height)
{
    if (_height != height) {
        _height = height;
        _rows = height / 8;
        createList();
        update();
        qDebug() << "Set height = " << height;
        this->setMinimumWidth( 100 + (_rows /2 - 1)* 120 );
    }
}

void UISelectChannel::createList()
{
    list.clear();
    for( unsigned int i = 0; i < _rows / 2; ++i )
    {
        for( int j = 0; j < 8; ++j )
        {
            list.append( Node( 20 + i * 120, 20 + j * 40 ) );
            list.append( Node( 60 + i * 120, 35 + j * 40 ) );
        }
    }
}

void UISelectChannel::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::white );
    p.setBrush((QBrush(Qt::red)));


    for( int i = 0; i < list.size(); ++i )
    {
        if( list[i].isOpen )
            p.setBrush( QColor( 0, 128, 0 ) );
        else
            p.setBrush( Qt::red );
        if( index == i )
            p.setPen( Qt::gray );
        else
            p.setPen( Qt::white );
        p.drawRect( list[i].x, list[i].y, 30, 30 );
        p.setBrush( Qt::white );

        p.drawText( list[i].x + 10 - (i/100 * 3), list[i].y + 20, QString::number( i ));
    }
    p.setPen( Qt::black );
}

void UISelectChannel::setCheckAll()
{
    for (int i = 0; i < list.size(); ++i) {
        list[i].isOpen = checkAll;
    }
    checkAll = checkAll?false:true;
    update();
}

void UISelectChannel::setCheckBoard()
{
    for (int i = 0; i < list.size(); ++i) {
        list[i].isOpen = (i%2)?checkAll:!checkAll;
    }
    checkAll = checkAll?false:true;
    update();
}

void UISelectChannel::mousePressEvent(QMouseEvent *event)
{
    int x = event->pos().x();
    int y = event->pos().y();

    for( int i = 0; i < list.size(); ++i )
    {
        if( ( x >= list[i].x && x <= list[i].x + 30 ) && ( y >= list[i].y && y <= list[i].y + 30 ) )
        {
            list[i].isOpen = list[i].isOpen?false:true;
            break;
        }
    }
    update();
}

void UISelectChannel::mouseMoveEvent(QMouseEvent *event)
{
    int x = event->pos().x();
    int y = event->pos().y();
    for( int i = 0; i < list.size(); ++i )
    {
        if( ( x >= list[i].x && x <= list[i].x + 30 ) && ( y >= list[i].y && y <= list[i].y + 30 ) )
        {
            index = i;
            update();
            return ;
        }
    }
    if( index != -1 )
        update();
    index = -1;
}

QByteArray* UISelectChannel::getData()
{
    _data->clear();
    char buffer;
    for( unsigned int i = 0; i < _rows; ++i )
    {
        for( int j = 0; j < 8; ++j )
        {
            buffer = ( buffer << 1) | ( list[i*8 + j].isOpen?1:0 );
        }
        qDebug() << QString::number( (unsigned int)buffer, 2 );
        _data->append( buffer );
    }
    return _data;
}

void UISelectChannel::clear()
{
    for( int i = 0; i < list.size(); ++i )
    {
        list[i].isOpen = false;
    }
}
