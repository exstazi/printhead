#ifndef UIGENERATEDATA
#define UIGENERATEDATA

#include <QDebug>
#include <QWidget>
#include <QByteArray>

class UIGenerateData: public QWidget
{
public:

    unsigned long getWidth() { return _width; }
    unsigned int getHeight() { return _height; }
    virtual QByteArray* getData() = 0;

    static unsigned long getConsuption( QByteArray* data ) {
        unsigned long consuption = 0;
        for (int i = 0; i < data->size(); ++i) {
            char byte = data->at(0);
            for( int j = 0; j < 8; ++j )
            {
                if (byte & (1<<j)) {
                    ++consuption;
                }
            }
        }
        return consuption;
    }


    void setWidth(unsigned long width) { _width = width; }
    void setHeight(unsigned int height) { _height = height; }


protected:
    unsigned long _width;
    unsigned int _height;

    QByteArray *_data;
};

#endif // UIGENERATEDATA

