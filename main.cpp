#include "viewterminal.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ViewTerminal w;
    w.show();

    return a.exec();
}
