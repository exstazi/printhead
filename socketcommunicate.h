#ifndef SOCKETCOMMUNICATE_H
#define SOCKETCOMMUNICATE_H

#include <QObject>
#include <QTcpSocket>
#include "communicate.h"

#define SIZE_HEADER 5
#define PORT 55255

class SocketCommunicate: public Communicate
{
    Q_OBJECT
public:
    SocketCommunicate(QString ipAddr);

    void setIpAddress(QString ipAddr);
    QString getIpAddress();

    void connectToHead();
    void disconnectFromHead();
    void writeCommand(uint8_t command, uint32_t param = 0, QByteArray* data = NULL);

    bool isConnect();


protected:
    void initSocket();

protected slots:
    void onConnected();
    void onDisconnected();
    void onReadyRead();
    void onError(QAbstractSocket::SocketError error);

protected:
    QTcpSocket *_client;
    QByteArray _buffer;
    QString _ipAddr;
};

#endif // SOCKETCOMMUNICATE_H
