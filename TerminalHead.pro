#-------------------------------------------------
#
# Project created by QtCreator 2016-01-25T15:05:52
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TerminalHead
TEMPLATE = app


SOURCES += main.cpp\
    viewterminal.cpp \
    socketcommunicate.cpp \
    ui/uiloadimage.cpp \
    ui/uiselectchannel.cpp \
    json.cpp \
    printhead.cpp

HEADERS  += \
    viewterminal.h \
    socketcommunicate.h \
    ui/uigeneratedata.h \
    ui/uiloadimage.h \
    ui/uiselectchannel.h \
    json.h \
    codeheadermessages.h \
    printhead.h \
    communicate.h

FORMS    += \
    viewterminal.ui

RESOURCES += \
    res.qrc
