#include "socketcommunicate.h"
#include <QDebug>

SocketCommunicate::SocketCommunicate(QString ipAddr)
{
    _ipAddr = ipAddr;
    initSocket();
}

void SocketCommunicate::setIpAddress(QString ipAddr)
{
    if (_ipAddr != ipAddr) {
        _client->close();
        _ipAddr = ipAddr;
    }
}

QString SocketCommunicate::getIpAddress()
{
    return _ipAddr;
}

void SocketCommunicate::initSocket()
{
    _client = new QTcpSocket(this);
    connect( _client, SIGNAL(connected()), this, SLOT(onConnected()) );
    connect( _client, SIGNAL(disconnected()), this, SLOT(onDisconnected()) );
    connect( _client, SIGNAL(readyRead()), this, SLOT(onReadyRead()) );
    connect( _client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)) );
}

void SocketCommunicate::connectToHead()
{
    if (!_client) {
        initSocket();
    }
    if (!_client->isOpen()) {
        _client->connectToHost(_ipAddr, PORT);
    }
}

void SocketCommunicate::disconnectFromHead()
{
    _stateConnect = false;
    _client->close();
    emit stateConnect(false);
}


void SocketCommunicate::writeCommand(uint8_t command,
                                     uint32_t param, QByteArray* data)
{
    if (_client && _client->isOpen() ) {
        QByteArray header;
        header.append( command );
        header.append((char*)&param, sizeof( param ) );
        if (data && data->size() > 0 ) {
            header.append( *data );
        }
        _client->write( header );
        _client->flush();
        header.clear();
    }
}

void SocketCommunicate::onConnected()
{
    _stateConnect = true;
    emit stateConnect(true);
}

void SocketCommunicate::onDisconnected()
{
    qDebug() << "on disconnect";
    _stateConnect = false;
    emit stateConnect(false);
}

void SocketCommunicate::onReadyRead()
{
    _buffer.append( _client->readAll() );
    qDebug() << "Size: " << _buffer.size();

    while (_buffer.size() >= SIZE_HEADER) {
        char command = _buffer.data()[0];
        uint32_t param = *(uint32_t*)(_buffer.data()+1);
        emit readyCommand( command, param );
        _buffer.remove(0, 5);
    }
}

void SocketCommunicate::onError(QAbstractSocket::SocketError error)
{
    _stateConnect = false;
    _client->close();
    emit stateConnect(false);
    qDebug() << "Socket error: " << error;
}
