#ifndef CODEHEADERMESSAGES
#define CODEHEADERMESSAGES

enum CodeHeaderMessages
{
    /**
     * Команда на передачу данных. Второй параметр - размер данных.
     */
    TRANSFER_DATA = 0x01,

    /**
     * Команда на одноктарную печать данных с задержкой.
     */
    SIGNAL_PRINT = 0x02,
    /**
     * Команда на переодическую печать данных с задержкой.
     */
    SIGNAL_PRINT_PERIOD = 0x03,
    /**
     * Команда на переодическую печать данных по сигналу енкодера.
     */
    SIGNAL_PRINT_ENCODER = 0x04,
    /**
     * Команда на остановку печати.
     */
    SIGNAL_PRINT_STOP = 0x05,
    /**
     * Команда включения триггера при печати.
     */
    USE_TRIGGER = 0x10,
    /**
     * Команда для задержки печати после включения головы.
     */
    USE_DELAY_FOR_PRINT = 0x11,

    /**
     * Команда на чтение статуса контроллера.
     */
    READ_STATUS = 0x20,
    READ_COUNT_PRINT = 0x21,
    READ_TEMP_1 = 0x22,
    READ_TEMP_2 = 0x23,
    READ_TEMP_3 = 0x24,
    READ_TEMP_ALL = 0x25,
    READ_STATE_HEAD = 0x26,
    READ_VERSION = 0x27,
    /**
     * Команды на запись параметров. Второй параметр - значение.
     */
    WRITE_PRINT_PERIOD = 0x30,
    WRITE_RESOLUTION = 0x31,
    WRITE_HEATING_TIME = 0x32,
    WRITE_IP_ADDRESS = 0x33,
    WRITE_TYPE_HEAD = 0x34,
    WRITE_TASK_HEATER = 0x35,
    /**
     * Команда на включение/отключения нагревателя.
     */
    WRITE_WORK_HEATER = 0x36,

    REBOOT = 0x60,
    CLOSE = 0x61,
    DISCONNECT = 0x62,

    GET_BASE_PARAM = 0x70,

    STATE_CONNECT = 0xA0,
    STATE_WARMING_UP = 0xA1,
    STATE_ERROR = 0xA2,
    STATE_READY_PRINT = 0xA3,
    STATE_PRINTING = 0xA4,

    HEAD_HEATS = 0xA0,
    PRINT_IS_NOT_READY = 0xA1,

    WRITE_FIRMWARE = 0xB0,
    UPDATE_FIRMWARE = 0xB1,
    UPDATE_WATCHDOG = 0xB2
};

#endif // CODEHEADERMESSAGES

