#include "viewterminal.h"
#include "ui_viewterminal.h"

#include <QFileDialog>
#include <QMovie>

#include <QDebug>

ViewTerminal::ViewTerminal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewTerminal)
{
    ui->setupUi(this);
    _head = NULL;
    _communicate = NULL;
    _timer = new QTimer(this);
    connect( _timer, SIGNAL(timeout()), this, SLOT(timerTick()) );

    _image = new UILoadImage();
    _channel = new UISelectChannel();
    _image->loadImage( ":/img/foxjet" );
    ui->scrollArea->setWidget( _image );

    ui->lbErrorConfiguration->setVisible( false );
    ui->loader->setVisible( false );
    ui->loader->setMovie( new QMovie( ":/gif/loadb" ) );

    //Control
    connect( ui->btnConnect, SIGNAL(clicked()), this, SLOT(connectToRPI()) );
    connect( ui->btnWrite, SIGNAL(clicked()), this, SLOT(writeToRPI()) );
    connect( ui->btnPrint, SIGNAL(clicked()), this, SLOT(printToPRI()) );
    connect( ui->btnPriodicals, SIGNAL(clicked()), this, SLOT(printPridicoals()) );
    connect( ui->btnPrintencoder, SIGNAL(clicked()), this, SLOT(printEncoder()) );
    connect( ui->btnStopPrint, SIGNAL(clicked(bool)), this, SLOT(stopPrint()) );
    //Menu
    connect(ui->btnLoadPicture, SIGNAL(clicked()), this, SLOT(loadPictures()));
    connect(ui->btnChangeMode, SIGNAL(clicked()), this, SLOT(changeMode()));
    connect(ui->btnConfigure, SIGNAL(clicked()), this, SLOT(changePageToConfigure()));
    connect(ui->btnUpdateFirmvare, SIGNAL(clicked()), this, SLOT(changePageToUpdateFirmvare()));
    connect(ui->btnHelp, SIGNAL(clicked()), this, SLOT(changePageToHelp()));
    connect(ui->btnCloseApp, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnFunction1, SIGNAL(clicked()), this, SLOT(onClickFunction1Btn()));
    connect(ui->btnFunction2, SIGNAL(clicked()), this, SLOT(onClickFunction2Btn()));
    //Configuration
    connect(ui->btnSaveConfiguration, SIGNAL(clicked()), this, SLOT(checkConfiguration()));
    connect(ui->btnShutDown, SIGNAL(clicked()), this, SLOT(shutdownHead()));
    connect(ui->btnReboot, SIGNAL(clicked()), this, SLOT(rebootHead()));
    connect(ui->cbDelayBeforePrint, SIGNAL(toggled(bool)), this, SLOT(changeTriggerHeatingTime(bool)));
    connect(ui->cbWorkHeater, SIGNAL(toggled(bool)), this, SLOT(changeTriggerTaskHeater(bool)));
    //Update
    connect(ui->btnUpdate, SIGNAL(clicked()), this, SLOT(updateFirmware()));

    ui->btnFunction1->setVisible( false );
    ui->btnFunction2->setVisible( false );

    loadConfiguration();
}

void ViewTerminal::loadConfiguration()
{
    QFile* file = new QFile( FN_CONFIGURATION );
    if (!file->open(QFile::ReadOnly | QFile::Text)) {
        delete file;
        qDebug() << "Configuration file not found";
        ui->btnConfigure->setText( "Back to main");
        ui->pages->setCurrentIndex( 1 );
        ui->header->setEnabled( false );
        return ;
    }

    QString json = QTextStream( file ).readAll();
    if (json.isEmpty()) {
        qDebug() << "Configuration file empty.";
        file->close();
        delete file;
        ui->btnConfigure->setText( "Back to main");
        ui->pages->setCurrentIndex( 1 );
        ui->header->setEnabled( false );
        return ;
    }
    bool ok;
    QtJson::JsonObject result = QtJson::parse(json, ok).toMap();
    if (!ok) {
         qDebug() << "Configuration file parse error.";
         file->close();
         delete file;
         ui->btnConfigure->setText( "Back to main");
         ui->pages->setCurrentIndex( 1 );
         ui->header->setEnabled( false );
         return ;
    }

    file->close();
    delete file;

    _communicate = new SocketCommunicate( result.value("ip-address").toString() );
    _head = new PrintHead( _communicate );
    connect( _head, SIGNAL(changeState(uint8_t)), this, SLOT(changeStateHead(uint8_t)) );
    connect( _head, SIGNAL(update()), this, SLOT(updateParamHead()) );
    connect( _head, SIGNAL(changePinState(uint8_t)), this, SLOT(changePinState(uint8_t)) );
    connect( _communicate, SIGNAL(stateConnect(bool)), this, SLOT(changeStatusPrintHead(bool)) );
    _image->setHeight( _head->getTypeHead()==0x01?256:128 );
    _channel->setHeight( _head->getTypeHead()==0x01?256:128  );

    _timer->start(result.value("period-update").toUInt());
     ui->txtPeriodUpdate->setText( result.value("period-update").toString() );

    _head->setTypeHead( result.value("type").toInt() );
    _head->setResolution( result.value("encoder-resolution").toUInt(),
                          result.value("print-resolution").toUInt() );
    _head->setPrintPeriod( result.value("print-period" ).toUInt() );
    _head->setHeatingTime( result.value("delay-before-print").toBool(),
                           result.value("heating-time").toUInt() );
    _head->setPrintWithTrigger( result.value("print-with-trigger").toBool() );
    _head->setTaskHeater( result.value("work-heater").toBool(),
                          result.value("task-heater").toInt() );
    _head->setIpAddress( result.value("ip-address").toString() );

    updateConfigurationHeadUI();
}

void ViewTerminal::saveConfiguration()
{
    QtJson::JsonObject configuration;
    configuration["type"] = _head->getTypeHead();
    configuration["ip-address"] = ui->txtIpAddress->text();
    configuration["encoder-resolution"] = QString::number(_head->getEncoderResolution());
    configuration["print-resolution"] = QString::number(_head->getPrintResolution());
    configuration["print-period"] = QString::number(_head->getPrintPeriod());
    configuration["print-with-trigger"] = _head->isPrintWithTrigger();
    configuration["delay-before-print"] = _head->isDelayBeforePrint();
    configuration["heating-time"] = QString::number(_head->getHeatingTime());
    configuration["work-heater"] = _head->isWorkHeater();
    configuration["task-heater"] = QString::number(_head->getTaskHeater());
    configuration["period-update"] = ui->txtPeriodUpdate->text();

    QFile* file = new QFile( FN_CONFIGURATION );
    file->open(QFile::WriteOnly);
    file->write( QtJson::serialize( configuration ) );
    file->close();
    delete file;

    ui->statusbar->showMessage( "Configuration file saved!", 10000 );
    qDebug() << "Configuration file saved!";

    updateConfigurationHeadUI();
}

void ViewTerminal::checkConfiguration()
{
    if (ui->txtIpAddress->text().size() < 8) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect ip address!");
        return ;
    }
    if (ui->txtEncoderReolution->text().toInt() < 1) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect encoder resolution!");
        return ;
    }
    if (ui->txtPrintResolution->text().toInt() < 1) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect print resolution!");
        return ;
    }
    if (ui->txtPeriod->text().toDouble() <= 0) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect period!");
        return ;
    }
    if (ui->cbDelayBeforePrint->isChecked() && ui->txtHeatingTime->text().toInt() < 1) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect heating time!");
        return ;
    }
    if (ui->cbWorkHeater->isChecked() && (ui->txtTaskHeater->text().toInt() > 100 || ui->txtTaskHeater->text().toInt() < 0)) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect task heater!");
        return ;
    }
    if (ui->txtPeriodUpdate->text().toDouble() < 1) {
        ui->lbErrorConfiguration->setVisible( true );
        ui->lbErrorConfiguration->setText( "Uncorrect period update!");
        return ;
    }


    ui->lbErrorConfiguration->setVisible( false );
    if (!_communicate) {
        qDebug() << "Create communicate and save configuration.";
        _communicate = new SocketCommunicate( ui->txtIpAddress->text() );
        _head = new PrintHead( _communicate );
        connect( _head, SIGNAL(changeState(uint8_t)), this, SLOT(changeStateHead(uint8_t)) );
        connect( _head, SIGNAL(update()), this, SLOT(updateParamHead()) );
        connect( _head, SIGNAL(changePinState(uint8_t)), this, SLOT(changePinState(uint8_t)) );
        connect( _communicate, SIGNAL(stateConnect(bool)), this, SLOT(changeStatusPrintHead(bool)) );
        _timer->start(ui->txtPeriodUpdate->text().toUInt());
        ui->header->setEnabled( true );
        _image->setHeight( _head->getTypeHead()==0x01?256:128 );
        _channel->setHeight( _head->getTypeHead()==0x01?256:128  );
    } else {
        _head->setIpAddress( ui->txtIpAddress->text() );
        ((SocketCommunicate*)_communicate)->setIpAddress( ui->txtIpAddress->text() );
        _timer->setInterval( ui->txtPeriodUpdate->text().toUInt() );

    }

    _head->setTypeHead( ui->listType->currentIndex() );
    _head->setResolution( ui->txtEncoderReolution->text().toUInt(),
                          ui->txtPrintResolution->text().toUInt() );
    _head->setPrintPeriod( ui->txtPeriod->text().toDouble() * 1000 );
    _head->setHeatingTime( ui->cbDelayBeforePrint->isChecked(),
                           ui->txtHeatingTime->text().toUInt() );
    _head->setPrintWithTrigger( ui->cbPrintWithTrigger->isChecked() );
    _head->setTaskHeater( ui->cbWorkHeater->isChecked(),
                          ui->txtTaskHeater->text().toInt() );

    saveConfiguration();
}

void ViewTerminal::updateConfigurationHeadUI()
{
    ui->lbNamePrintHead->setText( ((SocketCommunicate*)_communicate)->getIpAddress() );

    ui->listType->setCurrentIndex( _head->getTypeHead() );
    ui->txtIpAddress->setText( ((SocketCommunicate*)_communicate)->getIpAddress() );
    ui->txtEncoderReolution->setText( QString::number( _head->getEncoderResolution() ) );
    ui->txtPrintResolution->setText( QString::number(_head->getPrintResolution() ) );
    ui->cbPrintWithTrigger->setChecked( _head->isPrintWithTrigger() );
    ui->txtPeriod->setText( QString::number((double)_head->getPrintPeriod() / 1000 ) );
    ui->cbDelayBeforePrint->setChecked( _head->isDelayBeforePrint() );
    ui->txtHeatingTime->setText( QString::number(_head->getHeatingTime() ) );
    ui->cbWorkHeater->setChecked( _head->isWorkHeater() );
    ui->txtTaskHeater->setText( QString::number( _head->getTaskHeater() ) );

    ui->txtHeatingTime->setEnabled( _head->isDelayBeforePrint() );
    ui->lbHeatingTime->setEnabled( _head->isDelayBeforePrint() );
    ui->txtTaskHeater->setEnabled( _head->isWorkHeater() );
    ui->lbTaskHeater->setEnabled( _head->isWorkHeater() );


    _image->setHeight( _head->getCountNozzle() );
    _channel->setHeight( _head->getCountNozzle() );
}

void ViewTerminal::changeStateHead(uint8_t state)
{
    switch (state)
    {
        case PrintHead::WaitToConnect:
            ui->lbStatusHead->setText( "Disconnect...");
            ui->btnWrite->setEnabled( false );
            ui->btnPrint->setEnabled( false );
            ui->btnPriodicals->setEnabled( false );
            ui->btnPrintencoder->setEnabled( false );
            ui->btnStopPrint->setEnabled( false );
            ui->loader->setVisible( false );
            ui->loader->movie()->stop();
        break;

        case PrintHead::Connect:
            ui->lbStatusHead->setText( "Connected." );
            ui->btnWrite->setEnabled( true );
            ui->btnPrint->setEnabled( false );
            ui->btnPriodicals->setEnabled( false );
            ui->btnPrintencoder->setEnabled( false );
            ui->btnStopPrint->setEnabled( false );
            ui->loader->setVisible( false );
            ui->loader->movie()->stop();
        break;

        case PrintHead::WarmingUp:
            ui->lbStatusHead->setText( "Heating..." );
            ui->btnWrite->setEnabled( false );
            ui->btnPrint->setEnabled( false );
            ui->btnPriodicals->setEnabled( false );
            ui->btnPrintencoder->setEnabled( false );
            ui->btnStopPrint->setEnabled( false );
            ui->loader->setVisible( true );
            ui->loader->movie()->start();
        break;

        case PrintHead::Error:
            ui->lbStatusHead->setText( "Error. Buffer empty." );
            ui->btnWrite->setEnabled( true );
            ui->btnPrint->setEnabled( false );
            ui->btnPriodicals->setEnabled( false );
            ui->btnPrintencoder->setEnabled( false );
            ui->btnStopPrint->setEnabled( false );
            ui->loader->setVisible( false );
        break;

        case PrintHead::ReadyPrint:
            ui->lbStatusHead->setText( "Ready to print." );
            ui->btnWrite->setEnabled( true );
            ui->btnPrint->setEnabled( true );
            ui->btnPriodicals->setEnabled( true );
            ui->btnPrintencoder->setEnabled( true );
            ui->btnStopPrint->setEnabled( false );
            ui->loader->setVisible( false );
            ui->loader->movie()->stop();
        break;

        case PrintHead::Printing:
            ui->lbStatusHead->setText( "Printring..." );
            ui->btnWrite->setEnabled( false );
            ui->btnPrint->setEnabled( false );
            ui->btnPriodicals->setEnabled( false );
            ui->btnPrintencoder->setEnabled( false );
            ui->btnStopPrint->setEnabled( true );
            ui->loader->setVisible( true );
            ui->loader->movie()->start();
        break;


    }
}

void ViewTerminal::timerTick()
{
    _head->queryUpdate();
}

void ViewTerminal::updateParamHead()
{
    ui->lbCountPrint->setText( QString::number( _head->getCountPrint() ) );
    ui->lbConsuption->setText( QString::number( _head->getConsuption() ) );
    ui->lbUpdate->setText( QString::number( ui->lbUpdate->text().toUInt() + 1 ) );

    ui->lbTemperature->setText( QString::number( _head->getTemperature(0)) + " | "
                                + QString::number( _head->getTemperature(1)) + " | "
                                + QString::number( _head->getTemperature(2)));
}

void ViewTerminal::changeStatusPrintHead(bool status)
{
    if (status) {
        ui->btnConnect->setText( "Disconnect" );
        ui->txtStatus->setText( "Online" );
        ui->txtStatus->setStyleSheet("color: green;");
        ui->statusbar->showMessage( "Connected to RPI!", 10000 );
    } else {
        ui->btnConnect->setText( "Connect" );
        ui->txtStatus->setText( "Offline" );
        ui->txtStatus->setStyleSheet("color: red;");
        ui->statusbar->showMessage( "Disconnect. It may be due to an error.", 10000 );
    }
}

void ViewTerminal::changePinState(uint8_t state)
{
    if ((state & 0x01) == 0) {
        ui->lbHVOK->setStyleSheet( "background-color: green;");
    } else if ((state & 0x01) != 0) {
        ui->lbHVOK->setStyleSheet( "background-color: red;");
    }
    if ((state & 0x02) == 0) {
        ui->lbHTTERM->setStyleSheet( "background-color: green;");
    } else if ((state & 0x02) != 0) {
        ui->lbHTTERM->setStyleSheet( "background-color: red;");
    }
}

void ViewTerminal::resizeEvent(QResizeEvent *event)
{
    ui->horizontalLayoutWidget->setGeometry( QRect( 30,0, event->size().width() - 60, 30 ) );
    ui->headProperties->setGeometry( (event->size().width() / 2) - 175, (event->size().height() / 2) - 210, 351, 375 );
    ui->updateFirmware->setGeometry( (event->size().width() / 2) - 175, (event->size().height() / 2) - 100, 391, 121 );

    ui->gridWidget->setGeometry( 10, 40, event->size().width() - 20, 61 );
    ui->scrollArea->setGeometry( 10, 108, event->size().width() - 20, event->size().height() - 198 );
    ui->horizontalLayoutWidget_3->setGeometry( 30, event->size().height() - 88, event->size().width() - 40, 20 );

    ui->line_9->setGeometry( 11, 32, event->size().width() - 22, 16 );
    ui->horizontalLayoutWidget_2->setGeometry( 20, 10, event->size().width() - 40, 30 );
}

void ViewTerminal::connectToRPI()
{
    if ( ui->btnConnect->text() == "Connect" ) {
        _communicate->connectToHead();
        ui->loader->setVisible( true );
        ui->loader->movie()->start();
        ui->lbUpdate->setText( "0" );
    } else {
        _head->shutdown();
        _communicate->disconnectFromHead();
    }
}

void ViewTerminal::writeToRPI()
{
    _head->writeData( ((UIGenerateData*)ui->scrollArea->widget())->getData() );
    ui->loader->setVisible( true );
    ui->loader->movie()->start();
}

void ViewTerminal::printToPRI()
{
    _head->print();
}

void ViewTerminal::printPridicoals()
{
    _head->printdicals();
}

void ViewTerminal::printEncoder()
{
    _head->printWithEncoder();
}

void ViewTerminal::stopPrint()
{
    _head->stopPrint();
}

void ViewTerminal::rebootHead()
{
    _head->reboot();
}

void ViewTerminal::shutdownHead()
{
    _head->close();
}

void ViewTerminal::changeTriggerHeatingTime(bool status)
{
    ui->txtHeatingTime->setEnabled( status );
    ui->lbHeatingTime->setEnabled( status );
}

void ViewTerminal::changeTriggerTaskHeater(bool status)
{
    ui->txtTaskHeater->setEnabled( status );
    ui->lbTaskHeater->setEnabled( status );
}

void ViewTerminal::loadPictures()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("*.bmp"));

    if( file_name.size() < 5 )
    {
        ui->statusbar->showMessage( "File not found.", 10000 );
        return ;
    }
    ui->statusbar->showMessage( "Load file: " + file_name, 10000 );
    //ui->lbCurrentTask->setText( file_name.split( '/' ).last() );
    _image->loadImage( file_name );

    if( ui->scrollArea->widget() != _image )
    {
        ui->scrollArea->takeWidget();
        ui->scrollArea->setWidget( _image );
    }
    ui->btnWrite->setEnabled( true );
}

void ViewTerminal::changeMode()
{
    if (ui->scrollArea->widget() == _channel) {
        ui->btnChangeMode->setText( "Select channels" );
        ui->scrollArea->takeWidget();
        ui->scrollArea->setWidget( _image );
        ui->statusbar->showMessage( "Select mode: \"Load pictures\"", 10000 );
        ui->btnFunction1->setVisible( false );
        ui->btnFunction2->setVisible( false );
    } else {
        ui->btnChangeMode->setText( "Select picture" );
        ui->scrollArea->takeWidget();
        ui->scrollArea->setWidget( _channel);
        ui->statusbar->showMessage( "Select mode: \"Select channels\"", 10000 );
        ui->btnFunction1->setText( "Check All" );
        ui->btnFunction2->setText( "Check board" );
        ui->btnFunction1->setVisible( true );
        ui->btnFunction2->setVisible( true );
    }
}

void ViewTerminal::changePageToConfigure()
{
    if (ui->btnConfigure->text() == "Configure") {
        ui->pages->setCurrentIndex(1);
        if (_head->getState() == PrintHead::WaitToConnect) {

        } else {
            ui->btnShutDown->setEnabled(true);
            ui->btnReboot->setEnabled(true);
        }
        ui->btnConfigure->setText( "Back to main" );
        ui->btnHelp->setText( "Help" );
        ui->btnUpdateFirmvare->setText( "Update" );
    } else {
        ui->pages->setCurrentIndex(0);
        ui->btnConfigure->setText( "Configure" );
    }
}

void ViewTerminal::changePageToHelp()
{
    if (ui->btnHelp->text() == "Help") {
        ui->pages->setCurrentIndex(2);
        ui->btnHelp->setText( "Back to main" );
        ui->btnConfigure->setText( "Configure" );
        ui->btnUpdateFirmvare->setText( "Update" );
    } else {
        ui->pages->setCurrentIndex(0);
        ui->btnHelp->setText( "Help" );
    }
}

void ViewTerminal::changePageToUpdateFirmvare()
{
    if (ui->btnUpdateFirmvare->text() == "Update") {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("*"));

        if (fileName.length() < 5) {
            ui->statusbar->showMessage( "File not found: " + fileName, 10000 );
            return ;
        }

        ui->lbFileNameFirmware->setText( fileName.split('/').last() );
        QFile *file = new QFile( fileName );
        if (!file->open(QFile::ReadOnly)) {
            ui->statusbar->showMessage( "Не удалось открыть файл: " + fileName, 10000 );
            return ;
        }
        _head->writeFirmware(new QByteArray(file->readAll()));
        file->close();

        ui->pages->setCurrentIndex(3);
        ui->btnUpdateFirmvare->setText( "Back to main" );
        ui->btnHelp->setText( "Help" );
        ui->btnConfigure->setText( "Configure" );
    } else {
        ui->pages->setCurrentIndex(0);
        ui->btnUpdateFirmvare->setText( "Update" );
    }
}


void ViewTerminal::updateFirmware()
{
    _head->updateFirmware();
}

void ViewTerminal::onClickFunction1Btn()
{
    if (ui->scrollArea->widget() == _channel) {
        _channel->setCheckAll();
    }
}

void ViewTerminal::onClickFunction2Btn()
{
    if (ui->scrollArea->widget() == _channel) {
        _channel->setCheckBoard();
    }
}

ViewTerminal::~ViewTerminal()
{
    if (_head != NULL) {
        _head->shutdown();
    }
    if (_communicate != NULL) {
        _communicate->disconnectFromHead();
    }
    delete ui;
}
