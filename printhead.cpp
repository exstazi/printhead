#include "printhead.h"
#include <QDebug>

PrintHead::PrintHead(Communicate* communicate)
{
    _typeHead = 0x00;
    _encoderResolution = _printResolution = _printPeriod = _heatingTime = 0;
    _delayBeforePrint = _printWithTrigger = false;
    _isSynchronized = false;
    _countPrint = _consumption = 0;
    _communicate = communicate;

    _temperature = new double[3];
    _state = _communicate->isConnect() ? Connect : WaitToConnect;
    emit changeState( _state );

    connect( _communicate, SIGNAL(stateConnect(bool)), this, SLOT(stateConnect(bool)) );
    connect( _communicate, SIGNAL(readyCommand(uint8_t,uint32_t)), this, SLOT(readyCommand(uint8_t,uint32_t)) );
}

void PrintHead::writeData(QByteArray *data)
{
    qDebug() << "state = " << _state;
    _currentConsuption = UIGenerateData::getConsuption( data );
    if (_state == Connect || _state == ReadyPrint) {
        if (data->size() > SIZE_BUFFER) {
            _communicate->writeCommand( TRANSFER_DATA, SIZE_BUFFER, new QByteArray( data->right( SIZE_BUFFER )) );
        } else {
            _communicate->writeCommand( TRANSFER_DATA, data->size(), data );
        }
        _countPrint = _consumption = 0;
    }
}

void PrintHead::writeFirmware(QByteArray *data)
{
    _communicate->writeCommand( WRITE_FIRMWARE, data->size(), data);
}

void PrintHead::updateFirmware()
{
    if (_state == Connect || _state == ReadyPrint) {
        _communicate->writeCommand( UPDATE_FIRMWARE );
    }
}

void PrintHead::print()
{
    if (_state == ReadyPrint) {
        _communicate->writeCommand( SIGNAL_PRINT );
    }
}

void PrintHead::printdicals()
{
    if (_state == ReadyPrint) {
        _communicate->writeCommand( SIGNAL_PRINT_PERIOD );
    }
}

void PrintHead::printWithEncoder()
{
    if (_state == ReadyPrint) {
        _communicate->writeCommand( SIGNAL_PRINT_ENCODER );
    }
}

void PrintHead::stopPrint()
{
    if (_state == Printing) {
        _communicate->writeCommand( SIGNAL_PRINT_STOP );
    }
}

void PrintHead::setTypeHead(uint8_t type)
{
    if (_typeHead != type) {
        _typeHead = type;
        writeParam( WRITE_TYPE_HEAD, _typeHead );
    }
}

void PrintHead::setResolution(uint32_t encoderResolution, uint32_t printResolution)
{
    if (_encoderResolution != encoderResolution || _printResolution != printResolution) {
        _encoderResolution = encoderResolution;
        _printResolution = printResolution;
        writeParam( WRITE_RESOLUTION, _encoderResolution / ( _printResolution * INCH_TO_MM ) );
    }
}

void PrintHead::setPrintPeriod(uint32_t printPeriod)
{
    if (_printPeriod != printPeriod) {
        _printPeriod = printPeriod;
        writeParam( WRITE_PRINT_PERIOD, printPeriod );
    }
}

void PrintHead::setHeatingTime(bool delayBeforePrint, uint32_t heatingTime)
{
    if (_delayBeforePrint != delayBeforePrint) {
        _delayBeforePrint = delayBeforePrint;
        writeParam(USE_DELAY_FOR_PRINT, _delayBeforePrint);
    }
    if (_heatingTime != heatingTime) {
        _heatingTime = heatingTime;
        writeParam(WRITE_HEATING_TIME, _heatingTime);
    }
}

void PrintHead::setPrintWithTrigger(bool printWithTrigger)
{
    if (_printWithTrigger != printWithTrigger) {
        _printWithTrigger = printWithTrigger;
        writeParam(USE_TRIGGER, printWithTrigger);
    }
}

void PrintHead::setTaskHeater(bool workHeater, int task)
{
    if (_workHeater != workHeater) {
        _workHeater = workHeater;
        writeParam( WRITE_WORK_HEATER, _workHeater);
    }

    if (_taskHeater != task) {
        _taskHeater = task;
        writeParam( WRITE_TASK_HEATER, _taskHeater );
    }
}

void PrintHead::setIpAddress(QString addr)
{
    if (_ipAddr != addr) {
        _ipAddr = addr;
        QStringList lAddr = _ipAddr.split( '.' );
        qDebug() << lAddr;
        uint32_t as[4] = { lAddr[0].toUInt(), lAddr[1].toUInt(), lAddr[2].toUInt(), lAddr[3].toUInt() };
        QByteArray *arr = new QByteArray();
        arr->append((char*)&as[0], sizeof( unsigned int ) );
        arr->append((char*)&as[1], sizeof( unsigned int ) );
        arr->append((char*)&as[2], sizeof( unsigned int ) );
        arr->append((char*)&as[3], sizeof( unsigned int ) );

        if (_state == Connect || _state == ReadyPrint) {
            _communicate->writeCommand( WRITE_IP_ADDRESS, arr->size(), arr );
        }
    }
}

void PrintHead::queryUpdate()
{
    _communicate->writeCommand( GET_BASE_PARAM );
}

void PrintHead::reboot()
{
    _communicate->writeCommand( REBOOT );
}

void PrintHead::close()
{
    _communicate->writeCommand( CLOSE );
}

void PrintHead::shutdown()
{
    _communicate->writeCommand( DISCONNECT );
}

bool PrintHead::isDelayBeforePrint()
{
    return _delayBeforePrint;
}

bool PrintHead::isPrintWithTrigger()
{
    return _printWithTrigger;
}

bool PrintHead::isWorkHeater()
{
    return _workHeater;
}

uint8_t PrintHead::getTypeHead()
{
    return _typeHead;
}

uint32_t PrintHead::getEncoderResolution()
{
    return _encoderResolution;
}

uint32_t PrintHead::getPrintResolution()
{
    return _printResolution;
}

uint32_t PrintHead::getPrintPeriod()
{
    return _printPeriod;
}

uint32_t PrintHead::getHeatingTime()
{
    return _heatingTime;
}

int PrintHead::getTaskHeater()
{
    return _taskHeater;
}

uint32_t PrintHead::getCountNozzle()
{
    return _typeHead==1?256:128;
}

uint32_t PrintHead::getCountPrint()
{
    return _countPrint;
}

uint32_t PrintHead::getConsuption()
{
    return _countPrint * _currentConsuption * VOLUME;
}

double PrintHead::getTemperature(uint8_t sensor)
{
    return sensor < 3 ? _temperature[sensor] : 0;
}

uint8_t PrintHead::getState()
{
    return _state;
}

void PrintHead::writeAnotherCommand(uint8_t command, uint32_t param)
{
    if (_state != WaitToConnect || _state != Printing) {
        _communicate->writeCommand( command, param );
    }
}

void PrintHead::writeParam(uint8_t command, uint32_t param)
{
    if (_state == WaitToConnect || _state == Printing) {
        _isSynchronized = false;
    } else {
        _communicate->writeCommand( command, param );
    }
}

void PrintHead::stateConnect(bool state)
{
    if (state) {
        _state = Connect;
        if (!_isSynchronized) {
            _communicate->writeCommand( WRITE_TYPE_HEAD, _typeHead );
            _communicate->writeCommand( WRITE_RESOLUTION, _printResolution );
            _communicate->writeCommand( WRITE_PRINT_PERIOD, _printPeriod );
            _communicate->writeCommand( WRITE_HEATING_TIME, _heatingTime );
            _communicate->writeCommand( USE_DELAY_FOR_PRINT, _delayBeforePrint );
            _communicate->writeCommand( USE_TRIGGER, _printWithTrigger );
            _communicate->writeCommand( READ_STATE_HEAD );
            _isSynchronized = true;
        }
    } else {
        _state = WaitToConnect;
        _isSynchronized = false;
    }
    emit changeState( _state );
}

void PrintHead::readyCommand(uint8_t command, uint32_t param)
{
    qDebug() << "Command. " << (int)command << " " << param;
    switch (command)
    {
        case TRANSFER_DATA:
            _state = ReadyPrint;
        break;

        case SIGNAL_PRINT:
        case SIGNAL_PRINT_PERIOD:
        case SIGNAL_PRINT_ENCODER:
            _state = Printing;
        break;

        case SIGNAL_PRINT_STOP:
            _state = ReadyPrint;
        break;

        case READ_STATUS:
            emit changePinState( param );
        break;

        case READ_COUNT_PRINT:
            _countPrint = param;
        break;

        case READ_TEMP_1:
            _temperature[0] = (double)param * 0.0625;
        break;

        case READ_TEMP_2:
            _temperature[1] = (double)param * 0.0625;
        break;

        case READ_TEMP_3:
            _temperature[2] = (double)param * 0.0625;
            emit update();
        break;

        case READ_STATE_HEAD:
            _state = param - 0x9E;
        break;
    }

    emit changeState( _state );
}
